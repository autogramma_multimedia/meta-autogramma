SUMMARY = "Meta package for building an installable SDK for Autogramma OS"
LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit populate_sdk populate_sdk_qt5

# Add SDL2
TOOLCHAIN_HOST_TASK_append = " nativesdk-libsdl2 nativesdk-qtdeclarative nativesdk-qtdeclarative-tools "
TOOLCHAIN_TARGET_TASK_append = " \
	libsdl2 \
	libsdl2-dev \
	glew \
	glew-dev \
	glm-dev \
 "
